@extends('base') 

@section('main')

<form method="post" action="{{ route('notes.store') }}">
 @csrf
 <label for="title">Title: </label>
 <input type="text" name="title" />
 <br/>
 <br/>
 <label for="body">Text: </label>
 <input type="textarea" name="body" />
 <br/>
 <button type="submit" > Submit! </button>
</form>
@endsection 
